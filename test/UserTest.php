<?php
namespace App\Test;

use App\Greeter;
use App\User;

class UserTest extends \PHPUnit\Framework\TestCase {
	public function testUserTellName() {
		$user = new User(25,'John');
		self::assertStringContainsString(
			"My name is John",
			$user->tellName()
		);
	}

	public function testUserTellAge() {
		$user = new User(25,'John');
		self::assertStringContainsString(
			"I am 25",
			$user->tellAge()
		);
	}

    public function testUserAddFavoriteMovieReturn() {
		$user = new User(25,"John");
		self::assertTrue($user->addFavoriteMovie("The Wolf of Wall Street"));
	}

    public function testUserRemoveFavoriteMovie() {
		$user = new User(25,'John');
        $user->addFavoriteMovie("The Wolf of Wall Street");
		self::assertTrue($user->removeFavoriteMovie("The Wolf of Wall Street"));
	}
}

